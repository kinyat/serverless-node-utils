(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("aws-sdk"));
	else if(typeof define === 'function' && define.amd)
		define(["aws-sdk"], factory);
	else if(typeof exports === 'object')
		exports["serverless-node-utils"] = factory(require("aws-sdk"));
	else
		root["serverless-node-utils"] = factory(root["aws-sdk"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_0__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// Generated by CoffeeScript 1.10.0
module.exports = {
  100: 'Continue',
  101: 'Switching Protocols',
  200: 'OK',
  201: 'Created',
  202: 'Accepted',
  203: 'Non-Authoritative Information',
  204: 'No Content',
  205: 'Reset Content',
  206: 'Partial Content',
  207: 'Multi Status',
  208: 'Already Reported',
  226: 'IM Used',
  300: 'Multiple Choices',
  301: 'Moved Permanently',
  302: 'Found',
  303: 'See Other',
  304: 'Not Modified',
  305: 'Use Proxy',
  306: 'Switch Proxy',
  307: 'Temporary Redirect',
  308: 'Permanent Redirect',
  400: 'Bad Request',
  401: 'Unauthorized',
  402: 'Payment Required',
  403: 'Forbidden',
  404: 'Not Found',
  405: 'Method Not Allowed',
  406: 'Not Acceptable',
  407: 'Proxy Authentication Required',
  408: 'Request Time-out',
  409: 'Conflict',
  410: 'Gone',
  411: 'Length Required',
  412: 'Precondition Failed',
  413: 'Request Entity Too Large',
  414: 'Request-URI Too Large',
  415: 'Unsupported Media Type',
  416: 'Requested Range not Satisfiable',
  417: 'Expectation Failed',
  418: 'I\'m a teapot',
  421: 'Misdirected Request',
  422: 'Unprocessable Entity',
  423: 'Locked',
  424: 'Failed Dependency',
  426: 'Upgrade Required',
  428: 'Precondition Required',
  429: 'Too Many Requests',
  431: 'Request Header Fields Too Large',
  451: 'Unavailable For Legal Reasons',
  500: 'Internal Server Error',
  501: 'Not Implemented',
  502: 'Bad Gateway',
  503: 'Service Unavailable',
  504: 'Gateway Time-out',
  505: 'HTTP Version not Supported',
  506: 'Variant Also Negotiates',
  507: 'Insufficient Storage',
  508: 'Loop Detected',
  510: 'Not Extended',
  511: 'Network Authentication Required',
  CONTINUE: 100,
  SWITCHING_PROTOCOLS: 101,
  OK: 200,
  CREATED: 201,
  ACCEPTED: 202,
  NON_AUTHORITATIVE_INFORMATION: 203,
  NO_CONTENT: 204,
  RESET_CONTENT: 205,
  PARTIAL_CONTENT: 206,
  MULTI_STATUS: 207,
  ALREADY_REPORTED: 208,
  IM_USED: 226,
  MULTIPLE_CHOICES: 300,
  MOVED_PERMANENTLY: 301,
  FOUND: 302,
  SEE_OTHER: 303,
  NOT_MODIFIED: 304,
  USE_PROXY: 305,
  SWITCH_PROXY: 306,
  TEMPORARY_REDIRECT: 307,
  PERMANENT_REDIRECT: 308,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  PAYMENT_REQUIRED: 402,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  METHOD_NOT_ALLOWED: 405,
  NOT_ACCEPTABLE: 406,
  PROXY_AUTHENTICATION_REQUIRED: 407,
  REQUEST_TIMEOUT: 408,
  CONFLICT: 409,
  GONE: 410,
  LENGTH_REQUIRED: 411,
  PRECONDITION_FAILED: 412,
  REQUEST_ENTITY_TOO_LARGE: 413,
  REQUEST_URI_TOO_LONG: 414,
  UNSUPPORTED_MEDIA_TYPE: 415,
  REQUESTED_RANGE_NOT_SATISFIABLE: 416,
  EXPECTATION_FAILED: 417,
  IM_A_TEAPOT: 418,
  MISDIRECTED_REQUEST: 421,
  UNPROCESSABLE_ENTITY: 422,
  UPGRADE_REQUIRED: 426,
  PRECONDITION_REQUIRED: 428,
  LOCKED: 423,
  FAILED_DEPENDENCY: 424,
  TOO_MANY_REQUESTS: 429,
  REQUEST_HEADER_FIELDS_TOO_LARGE: 431,
  UNAVAILABLE_FOR_LEGAL_REASONS: 451,
  INTERNAL_SERVER_ERROR: 500,
  NOT_IMPLEMENTED: 501,
  BAD_GATEWAY: 502,
  SERVICE_UNAVAILABLE: 503,
  GATEWAY_TIMEOUT: 504,
  HTTP_VERSION_NOT_SUPPORTED: 505,
  VARIANT_ALSO_NEGOTIATES: 506,
  INSUFFICIENT_STORAGE: 507,
  LOOP_DETECTED: 508,
  NOT_EXTENDED: 510,
  NETWORK_AUTHENTICATION_REQUIRED: 511
};


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _httpStatus = __webpack_require__(1);

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _boolean = __webpack_require__(3);

var _boolean2 = _interopRequireDefault(_boolean);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Response {
  constructor(callback) {
    this._callback = callback;
  }

  get corsHeaders() {
    const {
      CORS
    } = process.env;

    if ((0, _boolean2.default)(CORS)) {
      return {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      };
    }

    return {};
  }

  successResponse({
    response = {}
  } = {}) {
    return this._callback(null, {
      statusCode: _httpStatus2.default.OK,
      headers: this.corsHeaders,
      body: JSON.stringify(response)
    });
  }

  resourceNotFoundResponse() {
    return this._callback(null, {
      statusCode: _httpStatus2.default.NOT_FOUND,
      headers: this.corsHeaders,
      body: JSON.stringify({
        error: 'Resource not found'
      })
    });
  }

  forbiddenResponse() {
    return this._callback(null, {
      statusCode: _httpStatus2.default.FORBIDDEN,
      headers: this.corsHeaders,
      body: JSON.stringify({
        error: 'Forbidden Access'
      })
    });
  }

  unauthorizedResponse() {
    return this._callback(null, {
      statusCode: _httpStatus2.default.UNAUTHORIZED,
      headers: this.corsHeaders,
      body: JSON.stringify({
        error: 'Unauthorized'
      })
    });
  }

  errorResponse({
    error = null,
    message = 'Error',
    statusCode = _httpStatus2.default.INTERNAL_SERVER_ERROR,
    allowMessages = [],
    allowCodes = []
  } = {}) {
    if (error) {
      console.log(error);
    }

    if (!error || allowMessages.indexOf(error.message) === -1 && allowCodes.indexOf(error.code) === -1 && allowCodes.indexOf(error.constructor.name) === -1) {
      return this._callback(null, {
        statusCode,
        headers: this.corsHeaders,
        body: JSON.stringify({
          error: message,
          code: 'InternalServerError'
        })
      });
    }

    return this._callback(null, {
      statusCode: _httpStatus2.default.BAD_REQUEST,
      headers: this.corsHeaders,
      body: JSON.stringify({
        error: error.message,
        code: error.code || error.constructor.name
      })
    });
  }
}
exports.default = Response;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var boolean = function boolean(value) {
  if (typeof value === 'string') {
    return (/^(true|t|yes|y|1)$/i.test(value.trim())
    );
  }

  if (typeof value === 'number') {
    return value !== 0;
  }

  if (typeof value === 'boolean') {
    return value;
  }

  return false;
};

module.exports = boolean;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.Email = exports.DynamoDBClient = exports.Response = exports.Event = undefined;

var _awsSdk = __webpack_require__(0);

var _awsSdk2 = _interopRequireDefault(_awsSdk);

var _Event = __webpack_require__(5);

var _Event2 = _interopRequireDefault(_Event);

var _Response = __webpack_require__(2);

var _Response2 = _interopRequireDefault(_Response);

var _DynamoDBClient = __webpack_require__(6);

var _DynamoDBClient2 = _interopRequireDefault(_DynamoDBClient);

var _Email = __webpack_require__(7);

var _Email2 = _interopRequireDefault(_Email);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const {
  IS_OFFLINE,
  AWS_ACCESS_KEY,
  AWS_SECRET_KEY
} = process.env;

if (!IS_OFFLINE) {
  _awsSdk2.default.config.update({ accessKeyId: AWS_ACCESS_KEY, secretAccessKey: AWS_SECRET_KEY });
}

const Event = exports.Event = _Event2.default;
const Response = exports.Response = _Response2.default;
const DynamoDBClient = exports.DynamoDBClient = _DynamoDBClient2.default;
const Email = exports.Email = _Email2.default;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _httpStatus = __webpack_require__(1);

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _Response = __webpack_require__(2);

var _Response2 = _interopRequireDefault(_Response);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const {
  IS_OFFLINE
} = process.env;

class Event {
  constructor(event, {
    callback = null
  } = {}) {
    this._event = event;

    if (callback) {
      this._response = new _Response2.default(callback);
    }

    this._processAuthorizer();
  }

  _processAuthorizer() {
    let {
      requestContext: {
        authorizer
      }
    } = this._event;

    if (IS_OFFLINE) {
      authorizer = authorizer.claims;
    }

    this._authorizer = authorizer || {};
  }

  get body() {
    try {
      return JSON.parse(this._event.body);
    } catch (error) {
      if (this._response) {
        return this._response.errorResponse({
          error,
          message: 'Invalid Request',
          statusCode: _httpStatus2.default.BAD_REQUEST
        });
      }
      console.log(error);
      throw error;
    }
  }

  get queryStringParameters() {
    return this._event.queryStringParameters;
  }

  get userId() {
    return this._authorizer.username;
  }

  get headers() {
    return this._event.headers;
  }

  get pathParameters() {
    return this._event.pathParameters;
  }
}
exports.default = Event;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _awsSdk = __webpack_require__(0);

var _awsSdk2 = _interopRequireDefault(_awsSdk);

var _boolean = __webpack_require__(3);

var _boolean2 = _interopRequireDefault(_boolean);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function awsCallbackHandler(resolve, reject) {
  return (error, data) => {
    if (error) {
      return reject(error);
    }
    resolve(data);
  };
}

function promisify(fn) {
  return (...args) => {
    return new Promise((resolve, reject) => {
      fn(...args, awsCallbackHandler(resolve, reject));
    });
  };
}

function getDynamoDBClient() {
  let options = {};

  if ((0, _boolean2.default)(process.env.IS_OFFLINE)) {
    options = {
      region: 'localhost',
      endpoint: 'http://dynamodb:8000',
      credentials: new _awsSdk2.default.Credentials('accessKey', 'secretKey')
    };
  }

  return new _awsSdk2.default.DynamoDB.DocumentClient(options);
}

const dynamoDBClient = getDynamoDBClient();

class DynamoDBClient {
  constructor({ tableName }) {
    this.params = {
      TableName: tableName
    };
  }

  _handleCallback(resolve, reject, result, error) {
    if (error) {
      reject(error);
    }
    resolve(result);
  }

  get(params) {
    return promisify(dynamoDBClient.get.bind(dynamoDBClient))(_extends({}, this.params, params));
  }

  put({ item }) {
    const params = _extends({}, this.params, {
      Item: item
    });

    return new Promise((resolve, reject) => {
      dynamoDBClient.put(params, (error, result) => {
        this._handleCallback(resolve, reject, result, error);
      });
    });
  }

  query({
    keyConditionExpression,
    expressionAttributeValues
  }) {
    const params = _extends({}, this.params, {
      KeyConditionExpression: keyConditionExpression,
      ExpressionAttributeValues: expressionAttributeValues
    });

    return new Promise((resolve, reject) => {
      dynamoDBClient.query(params, (error, result) => {
        this._handleCallback(resolve, reject, result, error);
      });
    });
  }

  update(params) {
    return new Promise((resolve, reject) => {
      dynamoDBClient.update(_extends({}, this.params, params), (error, result) => {
        this._handleCallback(resolve, reject, result, error);
      });
    });
  }
}
exports.default = DynamoDBClient;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _awsSdk = __webpack_require__(0);

var _awsSdk2 = _interopRequireDefault(_awsSdk);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const {
  IS_OFFLINE,
  EMAIL_TOPIC
} = process.env;

let sns = new _awsSdk2.default.SNS();

if (IS_OFFLINE) {
  sns = new _awsSdk2.default.SNS({
    region: 'localhost',
    endpoint: 'http://localstack:4575',
    credentials: new _awsSdk2.default.Credentials('accessKey', 'secretKey')
  });
}

class Email {
  constructor() {
    this.snsMessage = {
      htmlBody: 'No Body',
      subject: 'No Subject',
      source: 'noreply@rentopia.com',
      toAddresses: [],
      toUserIds: []
    };
  }

  setHtmlBody(htmlBody) {
    this.snsMessage.htmlBody = htmlBody;
    return this;
  }

  setSubject(subject) {
    this.snsMessage.subject = subject;
    return this;
  }

  setToAddresses(toAddresses) {
    this.snsMessage.toAddresses = toAddresses;
    return this;
  }

  addToAddress(toAddress) {
    this.snsMessage.toAddresses.push(toAddress);
    return this;
  }

  setToUserIds(toUserIds) {
    this.snsMessage.toUserIds = toUserIds;
    return this;
  }

  addToUserId(toUserId) {
    this.snsMessage.toUserIds.push(toUserId);
    return this;
  }

  send() {
    return new Promise((resolve, reject) => {
      sns.publish({
        Message: JSON.stringify(this.snsMessage),
        TargetArn: EMAIL_TOPIC
      }, (err, data) => {
        if (err) {
          return reject(err);
        }
        resolve(data);
      });
    });
  }
}
exports.default = Email;

/***/ })
/******/ ]);
});