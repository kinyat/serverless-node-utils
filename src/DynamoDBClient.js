import aws from 'aws-sdk'
import boolean from 'boolean'

function awsCallbackHandler (resolve, reject) {
  return (error, data) => {
    if (error) {
      return reject(error)
    }
    resolve(data)
  }
}

function promisify (fn) {
  return (...args) => {
    return new Promise((resolve, reject) => {
      fn(...args, awsCallbackHandler(resolve, reject))
    })
  }
}

function getDynamoDBClient () {
  let options = {}

  if (boolean(process.env.IS_OFFLINE)) {
    options = {
      region: 'localhost',
      endpoint: 'http://dynamodb:8000',
      credentials: new aws.Credentials('accessKey', 'secretKey')
    }
  }

  return new aws.DynamoDB.DocumentClient(options)
}

const dynamoDBClient = getDynamoDBClient()

export default class DynamoDBClient {
  constructor ({ tableName }) {
    this.params = {
      TableName: tableName
    }
  }

  _handleCallback (resolve, reject, result, error) {
    if (error) {
      reject(error)
    }
    resolve(result)
  }

  get (params) {
    return promisify(::dynamoDBClient.get)(
      {
        ...this.params,
        ...params
      }
    )
  }

  put ({ item }) {
    const params = {
      ...this.params,
      Item: item
    }

    return new Promise((resolve, reject) => {
      dynamoDBClient.put(params, (error, result) => {
        this._handleCallback(resolve, reject, result, error)
      })
    })
  }

  query ({
    keyConditionExpression,
    expressionAttributeValues
  }) {
    const params = {
      ...this.params,
      KeyConditionExpression: keyConditionExpression,
      ExpressionAttributeValues: expressionAttributeValues
    }

    return new Promise((resolve, reject) => {
      dynamoDBClient.query(params, (error, result) => {
        this._handleCallback(resolve, reject, result, error)
      })
    })
  }

  update (params) {
    return new Promise((resolve, reject) => {
      dynamoDBClient.update(
        {
          ...this.params,
          ...params
        },
        (error, result) => {
          this._handleCallback(resolve, reject, result, error)
        }
      )
    })
  }
}
