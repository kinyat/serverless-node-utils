import HTTPStatus from 'http-status'
import Response from './Response'

const {
  IS_OFFLINE
} = process.env

export default class Event {
  constructor (
    event,
    {
      callback = null
    } = {}
  ) {
    this._event = event

    if (callback) {
      this._response = new Response(callback)
    }

    this._processAuthorizer()
  }

  _processAuthorizer () {
    let {
      requestContext: {
        authorizer
      }
    } = this._event

    if (IS_OFFLINE) {
      authorizer = authorizer.claims
    }

    this._authorizer = authorizer || {}
  }

  get body () {
    try {
      return JSON.parse(this._event.body)
    } catch (error) {
      if (this._response) {
        return this._response.errorResponse({
          error,
          message: 'Invalid Request',
          statusCode: HTTPStatus.BAD_REQUEST
        })
      }
      console.log(error)
      throw error
    }
  }

  get queryStringParameters () {
    return this._event.queryStringParameters
  }

  get userId () {
    return this._authorizer.username
  }

  get headers () {
    return this._event.headers
  }

  get pathParameters () {
    return this._event.pathParameters
  }
}
