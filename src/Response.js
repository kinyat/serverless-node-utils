import HTTPStatus from 'http-status'
import boolean from 'boolean'

export default class Response {
  constructor (
    callback
  ) {
    this._callback = callback
  }

  get corsHeaders () {
    const {
      CORS
    } = process.env

    if (boolean(CORS)) {
      return {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      }
    }

    return {}
  }

  successResponse (
    {
      response = {}
    } = {}
  ) {
    return this._callback(null, {
      statusCode: HTTPStatus.OK,
      headers: this.corsHeaders,
      body: JSON.stringify(response)
    })
  }

  resourceNotFoundResponse () {
    return this._callback(null, {
      statusCode: HTTPStatus.NOT_FOUND,
      headers: this.corsHeaders,
      body: JSON.stringify({
        error: 'Resource not found'
      })
    })
  }

  forbiddenResponse () {
    return this._callback(null, {
      statusCode: HTTPStatus.FORBIDDEN,
      headers: this.corsHeaders,
      body: JSON.stringify({
        error: 'Forbidden Access'
      })
    })
  }

  unauthorizedResponse () {
    return this._callback(null, {
      statusCode: HTTPStatus.UNAUTHORIZED,
      headers: this.corsHeaders,
      body: JSON.stringify({
        error: 'Unauthorized'
      })
    })
  }

  errorResponse (
    {
      error = null,
      message = 'Error',
      statusCode = HTTPStatus.INTERNAL_SERVER_ERROR,
      allowMessages = [],
      allowCodes = []
    } = {}
  ) {
    if (error) {
      console.log(error)
    }

    if (
      !error || (
        allowMessages.indexOf(error.message) === -1 &&
        allowCodes.indexOf(error.code) === -1 &&
        allowCodes.indexOf(error.constructor.name) === -1
      )
    ) {
      return this._callback(null, {
        statusCode,
        headers: this.corsHeaders,
        body: JSON.stringify({
          error: message,
          code: 'InternalServerError'
        })
      })
    }

    return this._callback(null, {
      statusCode: HTTPStatus.BAD_REQUEST,
      headers: this.corsHeaders,
      body: JSON.stringify({
        error: error.message,
        code: error.code || error.constructor.name
      })
    })
  }
}
