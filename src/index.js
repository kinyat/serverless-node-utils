import aws from 'aws-sdk'
import Eve from './Event'
import Res from './Response'
import DDBC from './DynamoDBClient'
import Em from './Email'

const {
  IS_OFFLINE,
  AWS_ACCESS_KEY,
  AWS_SECRET_KEY
} = process.env

if (!IS_OFFLINE) {
  aws.config.update({ accessKeyId: AWS_ACCESS_KEY, secretAccessKey: AWS_SECRET_KEY })
}

export const Event = Eve
export const Response = Res
export const DynamoDBClient = DDBC
export const Email = Em
