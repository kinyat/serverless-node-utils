import aws from 'aws-sdk'

const {
  IS_OFFLINE,
  EMAIL_TOPIC
} = process.env

let sns = new aws.SNS()

if (IS_OFFLINE) {
  sns = new aws.SNS({
    region: 'localhost',
    endpoint: 'http://localstack:4575',
    credentials: new aws.Credentials('accessKey', 'secretKey')
  })
}

export default class Email {
  constructor () {
    this.snsMessage = {
      htmlBody: 'No Body',
      subject: 'No Subject',
      source: 'noreply@rentopia.com',
      toAddresses: [],
      toUserIds: []
    }
  }

  setHtmlBody (htmlBody) {
    this.snsMessage.htmlBody = htmlBody
    return this
  }

  setSubject (subject) {
    this.snsMessage.subject = subject
    return this
  }

  setToAddresses (toAddresses) {
    this.snsMessage.toAddresses = toAddresses
    return this
  }

  addToAddress (toAddress) {
    this.snsMessage.toAddresses.push(toAddress)
    return this
  }

  setToUserIds (toUserIds) {
    this.snsMessage.toUserIds = toUserIds
    return this
  }

  addToUserId (toUserId) {
    this.snsMessage.toUserIds.push(toUserId)
    return this
  }

  send () {
    return new Promise((resolve, reject) => {
      sns.publish({
        Message: JSON.stringify(this.snsMessage),
        TargetArn: EMAIL_TOPIC
      }, (err, data) => {
        if (err) {
          return reject(err)
        }
        resolve(data)
      })
    })
  }
}
